class SyntaxHighlighter {
	constructor(API, name, config) {
		this.API = API;
		this.name = name;
		this.config = config;
	}

	addInsertions() {
		this.API.addInsertion('publiiHead', this.addStyles, 1, this);
		this.API.addInsertion('publiiFooter', this.addScripts, 1, this);
	}

	addModifiers() {
		this.API.addModifier('postText', this.addLineNumberClass, 1, this);
		this.API.addModifier('postExcerpt', this.addLineNumberClass, 1, this);
   }

	addLineNumberClass(rendererInstance, text) {
		if (this.config.showLineNumbers) {
			 text = text.replace(/<pre class="((?!line-numbers)[\s\S]*?language-.*?)">/gmi, function(_, preClass) {
				  // If the line-numbers class is already present, don't add it again
				  if (preClass.includes('line-numbers')) {
						return `<pre class="${preClass}">`;
				  }
				  // Otherwise, add the line-numbers class
				  return `<pre class="${preClass} line-numbers">`;
			 });
		} else {
			 // If showLineNumbers is disabled, remove the line-numbers class if it exists
			 text = text.replace(/<pre class="([\s\S]*?)line-numbers([\s\S]*?)">/gmi, function(_, beforeClass, afterClass) {
				  return `<pre class="${beforeClass.trim()} ${afterClass.trim()}">`;
			 });
		}
		return text;
  }
  
  
	addStyles(rendererInstance) {
		if (this.shouldLoadHighlighter(rendererInstance)) {
			let styles = '';
			const theme = this.config.theme || 'black'; // Default theme
			styles += `<link rel="stylesheet" href="${rendererInstance.siteConfig.domain}/media/plugins/syntaxHighlighter/prism-${theme}.css" />`;

			// If inline color preview is enabled, load the CSS for it
			if (this.config.enableInlineColorPreview) {
				styles += `<link rel="stylesheet" href="${rendererInstance.siteConfig.domain}/media/plugins/syntaxHighlighter/prism-inline-color.css" />`;
			}

			return styles;
		}
		return '';
	}

	addScripts(rendererInstance) {
		if (this.shouldLoadHighlighter(rendererInstance)) {
			let scripts = `<script defer src="${rendererInstance.siteConfig.domain}/media/plugins/syntaxHighlighter/prism.js"></script>`;

			// If line numbers are enabled, load the JS for it
			if (this.config.showLineNumbers) {
				scripts += `<script defer src="${rendererInstance.siteConfig.domain}/media/plugins/syntaxHighlighter/prism-line-numbers.min.js"></script>`;
			}

		   // If clipboard are enabled, load the JS for it
			if (this.config.enableCopyToClipboard) {
				scripts += `<script defer src="${rendererInstance.siteConfig.domain}/media/plugins/syntaxHighlighter/clipboard.min.js"></script>`;
				scripts += `<script defer src="${rendererInstance.siteConfig.domain}/media/plugins/syntaxHighlighter/prism-copy-to-clipboard.min.js"></script>`;
			}
			
			// If inline color preview is enabled, load the JS for it
			if (this.config.enableInlineColorPreview) {
				scripts += `<script defer src="${rendererInstance.siteConfig.domain}/media/plugins/syntaxHighlighter/prism-inline-color.min.js"></script>`;
			}

			// If auto linker is enabled, load the JS for it
			if (this.config.enableAutoLinker) {
				scripts += `<script defer src="${rendererInstance.siteConfig.domain}/media/plugins/syntaxHighlighter/prism-autolinker.min.js"></script>`;
			}

			// If hidden characters is enabled, load the JS for it
			if (this.config.enableShowInvisibles) {
				scripts += `<script defer src="${rendererInstance.siteConfig.domain}/media/plugins/syntaxHighlighter/prism-show-invisibles.min.js"></script>`;
			}

			return scripts;
		}
		return '';
	}

	shouldLoadHighlighter(rendererInstance) {
		// Check whether to load on posts, everywhere (excluding 404 and search), or both pages
		let context = rendererInstance.globalContext.context;
		
		switch (this.config.loadOn) {
			 case 'posts':
				  return Array.isArray(context) && context.includes('post');
			 case 'all':
				  if (Array.isArray(context)) {
						// Check if context contains '404' or 'search'
						if (context.includes('404') || context.includes('search')) {
							 return false;
						} else {
							 return true;
						}
				  } else {
						return false;
				  }
			 default:
				  return false;
		}
  }
}

module.exports = SyntaxHighlighter;
