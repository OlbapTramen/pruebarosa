class DocSearchPlugin {
	constructor (API, name, config) {
		this.API = API;
		this.name = name;
		this.config = config;
	}

	addInsertions () {
		this.API.addInsertion('customSearchInput', this.addSearchInput, 1, this);
		this.API.addInsertion('publiiFooter', this.addScripts, 1, this);
		this.API.addInsertion('customHeadCode', this.addCSS, 1, this);
	}

	addSearchInput (rendererInstance, context) {
		let output = `<div id="docsearch"></div>`;

		if (this.config.cookieBannerIntegration) {
			output = `<div id="docsearch" style="display: none;"></div>`;
		}

		return output;
	}

	addScripts (rendererInstance, context) {
		let cookieBannerGroup = 'text/javascript';
		let consentScriptToLoad = '';
		
		if (this.config.cookieBannerIntegration) {
			cookieBannerGroup = 'gdpr-blocker/' + this.config.cookieBannerGroup.trim();
			consentScriptToLoad = `
			<script type="text/javascript">
			document.body.addEventListener('publii-cookie-banner-unblock-${this.config.cookieBannerGroup.trim()}', function () {
				document.getElementById('docsearch').style.display = '';
			}, false);
			</script>`;
		}

		return `
		   <script src="${rendererInstance.siteConfig.domain}/media/plugins/docSearch/docsearch.min.js"></script>
			<script type="${cookieBannerGroup}">
			docsearch({
				container: '#docsearch',
				appId: '${this.config.appId}',
				indexName: '${this.config.indexName}',
				apiKey: '${this.config.apiKey}',
				debug: false
			});
			</script>

			${consentScriptToLoad}
		`;
	}

	addCSS (rendererInstance, context) {
		return `<link rel="stylesheet" href="${rendererInstance.siteConfig.domain}/media/plugins/docSearch/docsearch.min.css">`;
	}
}

module.exports = DocSearchPlugin;
