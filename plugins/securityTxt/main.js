class SecurityTxt {
	constructor(API, name, config) {
		this.API = API;
		this.name = name;
		this.config = config;
	}

	addEvents() {
		this.API.addEvent('beforeRender', this.saveSecurityTxt, 1, this);
	}

	saveSecurityTxt(rendererInstance) {
		let output = [];

		if (this.config.contactInformation) {
			output.push("Contact: " + this.config.contactInformation);
		}

		if (this.config.expires) {
			let date = new Date(this.config.expires);
			let formattedDate = date.toISOString();
			output.push("Expires: " + formattedDate);
		}

		if (this.config.encryption) {
			output.push("Encryption: " + this.config.encryption);
		}

		if (this.config.acknowledgments) {
			output.push("Acknowledgments: " + this.config.acknowledgments);
		}

		if (this.config.policy) {
			output.push("Policy: " + this.config.policy);
		}

		if (this.config.hiring) {
			output.push("Hiring: " + this.config.hiring);
		}

		if (this.config.preferredLanguages) {
			output.push("Preferred-Languages: " + this.config.preferredLanguages);
		}

		if (this.config.canonical) {
			let domain = rendererInstance.siteConfig.domain;
			output.push(`Canonical: ${domain}/security.txt`);
		}

		if (Array.isArray(this.config.additionalDirectives)) {
			this.config.additionalDirectives.forEach((directive) => {
				 if (directive.name && directive.value) {
					  output.push(`${directive.name}: ${directive.value}`);
				 }
			});
	  }

		this.API.createFile('[ROOT-FILES]/security.txt', output.join('\n'), this);
	}
}

module.exports = SecurityTxt;
